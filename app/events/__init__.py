from flask import Blueprint


events = Blueprint('events', __name__, template_folder='../templates/events')

from .import controller, errors  # nopep8
