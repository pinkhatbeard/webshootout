from flask_wtf import FlaskForm
from wtforms import (StringField, SubmitField, SelectField)
from wtforms.validators import (InputRequired, EqualTo)
from wtforms.fields.html5 import DateField
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app.models import Team, Category


class RegisterEventForm(FlaskForm):
    # events = [(event.title, event.id) for event in Event.get()]
    # print(events)

    title = StringField('Title', validators=[
        InputRequired(message="Please enter a title.")])
    start_date = DateField('Start Date', format="%Y-%m-%d")
    end_date = DateField('End Date', format="%Y-%m-%d")
    teams = QuerySelectField(query_factory=Team.get, allow_blank=True)
    categories = QuerySelectField(query_factory=Category.get,
                                  allow_blank=True)
    submit = SubmitField('Submit')

    # def validate(self):
    #     if not super().validate():
    #         return False
    #     else:
    #         user = User.get(email=self.email.data.lower().strip())

    #         if user and user.check_password(self.password.data)
    #             return True
    #         else:
    #             self.password.errors.append("Invalid password.")
    #         return False
