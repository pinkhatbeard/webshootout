from flask import (render_template)
from flask_login import (login_required)

from . import events
from app.models import Event
from .forms import (RegisterEventForm)


@events.route('/events')
@login_required
def list():
    events = Event.get()
    return render_template('events/list.html', events=events)


@events.route('/event/<event_id>')
@login_required
def detail(event_id):
    event = Event.get(id=event_id)
    return render_template('events/detail.html', event=event)


@events.route('/event', methods=['GET', 'POST'])
@login_required
def add():
    form = RegisterEventForm()
    return render_template('events/new.html', form=form)
