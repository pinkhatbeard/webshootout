from flask import (render_template, flash, url_for, redirect, request)
from flask_login import (login_required)

from . import teams
from app.models import Team
from .forms import (RegisterTeamForm)


@teams.route('/teams/')
@login_required
def list():
    teams = Team.get()
    return render_template('teams/list.html', teams=teams)


@teams.route('/team/<team_id>')
@login_required
def detail(team_id):
    team = Team.get(id=team_id)
    return render_template('teams/detail.html', team=team)


@teams.route('/team', methods=['GET', 'POST'])
@login_required
def add():
    form = RegisterTeamForm()

    if request.method == 'GET':
        return render_template('teams/new.html', form=form)

    elif request.method == 'POST':
        if form.validate():
            if form.leader.data not in form.members.data:
                form.members.data.append(form.leader.data)
            new_team = Team(
                name=form.name.data.title().strip(),
                description=form.description.data,
                members=form.members.data,
                leader=form.leader.data)
            new_team = Team.create(new_team)
            flash("Form all good.")
            flash("Registered {}.".format(new_team.email))
            return redirect(url_for('.detail', team_id=new_team.id))
        else:
            flash("Form didn't validate.")
            return render_template('teams/new.html', form=form)


@teams.route('/team/<team_id>/edit', methods=['GET', 'POST'])
@login_required
def edit(team_id):
    form = RegisterTeamForm()

    if request.method == 'GET':
        team = Team.get(id=team_id)
        # form.populate_obj(team)

        return render_template('teams/new.html', form=form)

