from functools import wraps

from flask import (flash, url_for, redirect)
from flask_login import current_user


def format_datetime(date, dt_format='%Y-%m-%d'):
    return date.strftime(dt_format)


def anon_required(func):
    """
    Decorator that is the antithesis of @login_required.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:
            flash("Please logout to use this feature.")
            return redirect(url_for('main.index'))
        else:
            return func(*args, **kwargs)
    return wrapper


def admin_required(func):
    """Admin privlidges required to access admin pages."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated():  # and current_user.roles.is_admin:
            return func(*args, **kwargs)
        else:
            flash("You must be an admin to access this page.")
            return redirect(url_for('index'))
    return wrapper


def serializer(secret_key=None):
    from flask import current_app
    if secret_key is None:
        secret_key = current_app.config['SECRET_KEY']
    return URLSafeSerializer(secret_key)


def timed_serializer(secret_key=None):
    from flask import current_app
    if secret_key is None:
        secret_key = current_app.config['SECRET_KEY']
    return URLSafeTimedSerializer(secret_key)
