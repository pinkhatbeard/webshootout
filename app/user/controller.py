import datetime
from flask import (render_template, g, request, redirect, url_for, flash,
                   session, abort)
from flask_login import (login_user, logout_user, current_user, login_required)
from app import lm
from . import user
from .forms import (LoginForm, RegisterUserForm, ForgotPasswordForm,
                    ResetPasswordForm)
from app.models import User
from app.utils import anon_required


@lm.user_loader
def load_user(user_id):
    user = User.get(id=user_id)
    return user


@user.before_request
def before_request():
    g.user = current_user


@user.route('/login', methods=['GET', 'POST'])
@anon_required
def login():
    form = LoginForm()

    if request.method == 'GET':
        return render_template('login.html', form=form)
    elif request.method == 'POST':
        if form.validate():
            user = User.get(email=form.email.data.lower().strip())
            if user.confirmed and user.is_active:
                if login_user(user):
                    user.last_seen = datetime.datetime.utcnow()
                    user.save()
                    return redirect(request.args.get('next') or
                                    url_for('.profile', user_id=user.get_id()))
                else:
                    flash('Your account is marked inactive.')
                    return render_template('login.html', form=form)
            else:
                flash('Please confirm your email.')
                return render_template('login.html', form=form)
        else:
            flash('Your username and password pair did not match.')
            return render_template('login.html', form=form)


@user.route('/logout')
@login_required
def logout():
    logout_user()
    session.clear()
    flash("You've been logged out.")
    return redirect(url_for('main.index'))


@user.route('/register', methods=['GET', 'POST'])
@anon_required
def register():
    form = RegisterUserForm()
    if request.method == 'GET':
        return render_template('register.html', form=form)

    elif request.method == 'POST':
        if form.validate():
            new_user = User(
                firstname=form.firstname.data.title(),
                lastname=form.lastname.data.title(),
                email=form.email.data.lower().strip(),
                password=form.password.data,
                )
            new_user = User.create(new_user)
            flash("Form all good.")
            flash("Registered {}.".format(new_user.email))
            flash("Please confirm your email address by checking your email.")
            return redirect(url_for('.login'))
        else:
            flash("Form didn't validate.")
            return render_template('register.html', form=form)


@user.route('/activate/<payload>')
@anon_required
def activate_user(payload):
    user_email = User.check_activation_link(payload)
    if not user_email:
        return abort(404)
    user = User.get(email=user_email)
    if user:
        if not user.confirmed:
            user.activate()
            user.svae()
            flash('Your account has been activated.')
        else:
            flash('Your accoutn was already active.')
        return redirect(url_for('.login'))
    else:
        return abort(404)


@user.route('/forgotpassword', methods=['GET', 'POST'])
@anon_required
def forgot_password():
    form = ForgotPasswordForm()

    if request.method == 'GET':
        return render_template('forgot_password.html', form=form)

    elif request.method == 'POST':
        if form.validate():
            user = User.get(email=form.email.data.lower().strip())
            if not user:
                flash("That email does not exist, please try again.")
                return render_template('forgot_password.html', form=form)

            payload = User.get_password_reset_link(user)
            User.email_password_reset(user, payload)
            flash("Password reset email has been sent.  " +
                  "Link expires in 24 horus.")
            return redirect(url_for('main.index'))
        else:
            return render_template('forgot_password.html', form=form)


# @user.route('/resetpassword/', methods=['GET'], defaults={'payload': None})
# @user.route('/resetpassword/<payload>', methods=['POST'])
# @anon_required
# def reset_password(payload):
#     form = ResetPasswordForm()
#     user, payload_hash = User.check_reset_link(payload)

#     if request.method == 'GET':
#         if not user:
#             flash('Token incorrect or has expired. Please try again.')
#             return redirect(url_for('.forgot_password'))
#         elif user and payload_hash != user.password[:10]:
#             flash('Token has been previously used. Please try again')
#             return redirect(url_for('.forgot_password'))
#         else:
#             return render_template('reset_password.html', form=form)

#     elif request.method == 'POST':
#         pass


@user.route('/profile/<user_id>')
def profile(user_id):
    user = User.get_or_404(id=user_id)
    return render_template('profile.html', user=user)
