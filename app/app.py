import click

from app import create_app


app = create_app('development')


@app.cli.command()
def create_admin():
    click.echo('Eventually this will make an admin.')
