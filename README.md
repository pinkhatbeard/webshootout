# Web-Framework Shootout

## Running
```
git clone git@gitlab.com:pinkhatbeard/webshootout.git
cd webshootout
pip install -r requirements.txt
FLASK_DEBUG=1 FLASK_APP=app/app.py flask db init
FLASK_DEBUG=1 FLASK_APP=app/app.py flask db migrate
FLASK_DEBUG=1 FLASK_APP=app/app.py flask db upgrade
FLASK_DEBUG=1 FLASK_APP=app/app.py flask run
```

## User Story
User Story:
Harold’s company holds an annual ShipIt Day, where they work on a project that
benefits the company. At the end of the day, all employees get to vote on the
project. The votes consist of three weighted categories (for example, usefulness,
awesomeness, and completeness) per team, but a team cannot vote for itself. The
person who receives the best score (see algorithm for details) wins the day and
receives a prize.

## Requirements
- [X] Users should be able to register with the app
- [X] Admins should be able to create user accounts
- [X] The system should only allow one account per email address
- [X] Only admins should be able to create new events
- [ ] An Event should have a Title, Start Date, End Date, and one or more weighted
      categories (category name, weight decimal)
- [X] Admins should be able to edit existing events
- [X] The Team object should consist of: title field, rich text description, members
      (picked from list of users), and leader (select one user)
- [ ] The system should list all teams alphabetically for any specific event. Teams
      should be listed with title, description, member list, and team leader
- [X] Admins should be able to delete teams
- [ ] For an event, users should only be able to cast one vote per category per team
- [ ] The system should save votes
- [ ] Users should not be able to vote for a team they belong to
- [ ] A vote in each category for each team is required for a vote to be saved
- [ ] The system should tally up the results for each team based on a set of rules (see details)
    - [ ] A vote for a "1" = 1 point * weight of category
    - [ ] A vote for a "2" = 2 points * weight of category
    - [ ] A vote for a "3" = 3 points * weight of category
- [ ] The system should only display the final results once either all users have
      voted or the voting time frame is closed
- [ ] All logged in users should be able to see the results
- [ ] The results should be tallied using the specified formula (see ticket)
    - [ ] Take the average vote for every team in every category
    - [ ] For each average of the above (i) take the average
    - [ ] Convert average in the above (ii) to percentage
    - [ ]  Final Result = (100*(AverageTotal))/3
    - [ ] where 3 is the highest weight of category

## Low Priority
- [X] The system should display a confirmation message after submitting registration data
- [X] All users should have to log in to view any of the app data
- [X] Users should be able to log out of the application
- [ ] The app should list past events
- [ ] If there are multiple events planned, the app should display the event with
      the closest Start Date
- [ ] Any logged-in user should be able to create a team for a specific event
- [ ] The team title, rich text descriptions, and team leader should only be editable
      by admins or the team leader
- [ ] Users should be able to see the whole description for each team on the voting page
- [ ] The results should be shown in the form of a list of teams ordered based on the number
      of points they have received. Results should list: team name, total points, and names of
      the team members.

## Nice to Have
- [X] Admins should be able to review and approve registration submissions
- [X] Admins should be able to disable user accounts
- [ ] Upon approval by the admin, users should receive a notification email prompting
      them to log in
- [ ] Users should be able to reset their password
- [ ] The system should display user's name as a drop-down action menu with items:
      Change Password, Log Out, My Team (for team leaders only)
- [ ] Users should be able to switch teams from their my team menu
- [ ] On registration, users can select a pre-existing team
- [ ] The ranking scale for the categories should be able to be defined per event
      by the admin (e.g., "low," "medium," "high")
- [X] Admins should be able to delete events
- [ ] Admins should be able to close team creation on an Event
- [ ] Admins should be able to open/close the voting time frame for an Event
- [ ] Admins should be able to close team switching on an event
- [ ] Votes should be updated in real time (e.g., via AJAX) as they make selections
      in each category for a team
- [ ] The team the user is on should display, but it should be grayed out/not selectable
- [ ] The top 3 teams in the result list should be styled so they stand out
